package com.example.demo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
    @Data
    @Table(name = "users")
    @Entity
    @NoArgsConstructor
    public class UserEntity implements Serializable {
        @Id
        String id;
        String firstName;
        String lastName;

        @ElementCollection
        List<String> groupIds;

        @JsonCreator
        public UserEntity(@JsonProperty("id") String id,
                          @JsonProperty("firstName") String firstName,
                          @JsonProperty("lastName") String lastName) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.groupIds = new ArrayList<>();
        }

    }
