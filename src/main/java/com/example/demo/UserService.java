package com.example.demo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Service
@AllArgsConstructor
public class UserService {
    @Autowired
    UserRepository userRepository;
    public UserEntity addNewUser(CreateUser createUser) {
        UserEntity userEntity = new UserEntity(
                UUID.randomUUID().toString(),
                createUser.getFirstName(),
                createUser.getLastName()
        );
         return userRepository.save(userEntity);

    }

    public List<UserEntity> getAll() {
      return (List<UserEntity>) userRepository.findAll();
    }

    public UserEntity deleteUser(String id) {
        UserEntity userEntity = userRepository.getById(id);
        System.out.println(userEntity);
        userRepository.deleteById(id);
        return userEntity;
    }

    public UserEntity updateUser(String id, UpdateUser updateUser) {
        UserEntity userEntity = userRepository.getById(id);
        userEntity.setFirstName(updateUser.firstName);
        userEntity.setLastName(updateUser.lastName);
        userRepository.save(userEntity);
        return userEntity;
    }

    public UserEntity addGroupToUser(String id, GroupEntity group) {
        UserEntity userEntity = userRepository.getById(id);
        userEntity.getGroupIds().add(group.getId());
        userRepository.save(userEntity);
        return userEntity;
    }
}