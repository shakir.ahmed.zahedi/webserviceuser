package com.example.demo;
import java.util.List;

public class UserDTO {
    String firstName;
    String lastName;
   List<String> groupIds;

    public UserDTO(String firstName, String lastName,List<String> groupIds) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.groupIds = groupIds;
    }

}
