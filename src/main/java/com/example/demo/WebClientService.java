package com.example.demo;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
@Service
public class WebClientService {

    public List<GroupEntity> getAllGroups() {
        WebClient webClient = WebClient.create("http://localhost:8091/groups");

        return webClient.get()
                .retrieve()
                .bodyToFlux(GroupEntity.class)
                .collectList()
                .block();
    }

    public GroupEntity findByGroupName(String name) {
        WebClient webClient = WebClient.create("http://localhost:8091/groups/"+name);
        return webClient.get()
                .retrieve()
                .bodyToMono(GroupEntity.class)
                .block();

    }

}
