package com.example.demo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/users")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    @Autowired
    UserService userService;
  WebClientService webClientService;
    @PostMapping("/add")
    public UserEntity addNewUser(@RequestBody CreateUser createUser) throws IOException {
        UserEntity userEntity = userService.addNewUser(createUser);

        return userEntity;
    }
    @GetMapping()
    public List<UserEntity> all(){
        return userService.getAll().stream()
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{id}")
    public UserEntity deleteUser(@PathVariable("id") String id){
        UserEntity userEntity = userService.deleteUser(id);
        return  userEntity;
    }
    @PutMapping("/{id}")
    public UserEntity updateUser(@PathVariable("id") String id,@RequestBody UpdateUser updateUser){
        UserEntity userEntity = userService.updateUser(id,updateUser);
        return  userEntity;
    }

    @GetMapping("/getAllGroups")
    public List<GroupEntity> getAllGroups() {
        return webClientService.getAllGroups();
    }

    @GetMapping("/addGroupToUser/{id}")
    public UserEntity addGroupToUser(@PathVariable("id") String id, @RequestParam String name) {
        GroupEntity group = webClientService.findByGroupName(name);
        return userService.addGroupToUser(id, group);
    }


}
